package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	//"log"

	//"github.com/golang/protobuf/ptypes/empty"
	"github.com/prometheus/common/log"
	cad "gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

func main() {
	client := cad.NewCadTwirpProtobufClient("http://localhost:1234", &http.Client{})

	companyInfo, err := client.QueryCompanyInfoByCompanyName(
		context.Background(),
		&cad.QueryCompanyInfoByCompanyNameRequest {
			CompanyName: "Adocu.com",
		},
	)

	if err != nil {
		log.Debug(err)
	}

	companyInfoJSON, err := json.MarshalIndent(companyInfo, "", "\t")
	if err != nil {
		log.Debug(err)
	}
	fmt.Println("companyInfo:")
	fmt.Println(string(companyInfoJSON))
	fmt.Println("==================================================")
}