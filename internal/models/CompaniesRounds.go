package models

//CompaniesRounds ...
type CompaniesRounds struct {
	// companies       Companies
	Permalink       string `gorm:"primary_key"`
	Name            string `gorm:"type:varchar(255);"`
	CategoryCode    string `gorm:"type:varchar(255);"`
	FundingTotal    int64
	CountryCode     string `gorm:"type:varchar(255);"`
	StateCode       string `gorm:"type:varchar(255);"`
	Region          string `gorm:"type:varchar(100);"`
	City            string `gorm:"type:varchar(100);"`
	FundingRounds   int64
	FoundedAt       string `gorm:"type:varchar(10);"`
	FoundedMonth    string `gorm:"type:varchar(10);"`
	FoundedQuarter  string `gorm:"type:varchar(10);"`
	FoundedYear     int64
	FirstFundedAt   string `gorm:"type:varchar(10);"`
	LastFundedAt    string `gorm:"type:varchar(10);"`
	LastMilestoneAt string `gorm:"type:varchar(10);"`
	// rounds              Rounds
	CompanyPermalink    string `gorm:"type:varchar(255);"`
	CompanyName         string `gorm:"type:varchar(255);"`
	CompanyCategoryCode string `gorm:"type:varchar(255);"`
	CompanyCountryCode  string `gorm:"type:varchar(255);"`
	CompanyStateCode    string `gorm:"type:varchar(255);"`
	CompanyRegion       string `gorm:"type:varchar(255);"`
	CompanyCity         string `gorm:"type:varchar(255);"`
	FundingRoundType    string `gorm:"type:varchar(255);"`
	FundedAt            string `gorm:"type:varchar(255);"`
	FundedMonth         string `gorm:"type:varchar(255);"`
	FundedQuarter       string `gorm:"type:varchar(255);"`
	FundedYear          int
	RaisedAmountUsd     int
}
