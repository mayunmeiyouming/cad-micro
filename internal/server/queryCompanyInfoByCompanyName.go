package server

import (
	context "context"

	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

// QueryCompanyInfoByCompanyName 该接口用于搜索页的精准查找，通过输入的公司名查找公司信息
func (s *Server) QueryCompanyInfoByCompanyName(ctx context.Context, request *cad.QueryCompanyInfoByCompanyNameRequest) (*cad.CompanyInfo, error) {
	company, err := s.Repository.QueryCompanyInfoByCompanyName(request.CompanyName)
	if err != nil {
		return nil, err
	}

	companyInfo := &cad.CompanyInfo{
		CompanyId:    company.Permalink,
		CompanyName:  company.Name,
		CategoryCode: company.CategoryCode,
	}

	return companyInfo, nil
}
