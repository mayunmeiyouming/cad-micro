package server

import (
	context "context"
	"errors"
	"log"
	"strconv"

	"github.com/golang/protobuf/ptypes/wrappers"

	// "gitlab.com/mayunmeiyouming/cad-micro/internal/models"
	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

//QueryInvestmentListByCompanyID ...
func (s *Server) QueryInvestmentListByCompanyID(ctx context.Context, request *cad.QueryInvestmentListByCompanyIDRequest) (*cad.InvestmentTransactionConnection, error) {
	var limit, offset, count int
	log.Println("正在查询投资事件总数")
	length, err := s.Repository.QueryInvestmentCountByCompanyID(request.CompanyId)
	if err != nil {
		return nil, err
	}
	count = *length
	log.Println("投资事件总数: ", *length)
	if count == 0 {
		return nil, errors.New("no datas")
	}
	if request.First != nil && request.Last != nil {
		return nil, errors.New("param first and last can't exist in same time?")
	}

	//把first和after的值转为limit和offset
	if request.First != nil {
		limit, offset = s.ChangeFirstAfterToLimitOffset(request.First, request.After)
	}

	//把last和before的值转为limit和offset
	if request.Last != nil {
		limit, offset = s.ChangeLastBeforeToLimitOffset(request.Last, request.Before, count)
	}

	investmentsDB, err := s.Repository.QueryInvestmentListByCompanyID(request.CompanyId, limit, offset)
	if err != nil {

		return nil, err
	}

	investmentConn := &cad.InvestmentTransactionConnection{}
	pageInfo := &cad.PageInfo{}

	if offset > 1 {
		pageInfo.HasPreviousPage = true
	} else {
		pageInfo.HasPreviousPage = false
	}

	if offset < count && offset+limit < count {
		pageInfo.HasNextPage = true
	} else {
		pageInfo.HasNextPage = false
	}
	pageInfo.StartCursor = &wrappers.StringValue{Value: strconv.Itoa(offset + 1)}
	pageInfo.EndCursor = &wrappers.StringValue{Value: strconv.Itoa(offset + limit)}

	investmentConn.PageInfo = pageInfo
	investmentConn.TotalCount = int64(len(investmentsDB))

	for i, investment := range investmentsDB {
		node := &cad.InvestmentTransaction{
			InvestorInfo: &cad.CompanyInfo{
				CompanyName:  investment.InvestorName,
				CompanyId:    investment.InvestorPermalink,
				CategoryCode: investment.InvestorCategoryCode,
			},
			FinancingInfo: &cad.FinancingInfo{
				CompanyId:        investment.CompanyPermalink,
				FundingRoundType: investment.FundingRoundType,
				FundedAt:         investment.FundedAt,
				RaisedAmountUsd:  investment.RaisedAmountUsd,
				CompanyName:      investment.CompanyName,
			},
		}
		investmentConn.Nodes = append(investmentConn.Nodes, node)

		edge := &cad.InvestmentTransactionEdge{
			Node: &cad.InvestmentTransaction{
				InvestorInfo: &cad.CompanyInfo{
					CompanyName:  investment.InvestorName,
					CompanyId:    investment.InvestorPermalink,
					CategoryCode: investment.InvestorCategoryCode,
				},
				FinancingInfo: &cad.FinancingInfo{
					CompanyId:        investment.CompanyPermalink,
					FundingRoundType: investment.FundingRoundType,
					FundedAt:         investment.FundedAt,
					RaisedAmountUsd:  investment.RaisedAmountUsd,
					CompanyName:      investment.CompanyName,
				},
			},
			Cursor: strconv.Itoa(offset + i + 1),
		}
		investmentConn.Edges = append(investmentConn.Edges, edge)
	}
	return investmentConn, nil

}
